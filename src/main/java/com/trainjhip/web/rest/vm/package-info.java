/**
 * View Models used by Spring MVC REST controllers.
 */
package com.trainjhip.web.rest.vm;
